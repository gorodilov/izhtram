using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace IzhTramClient.Extensions
{
    public static class DictionaryExtensions
    {
        public static StringContent ToStringContent(this Dictionary<string, string> dictionary)
        {
            var parametersString = string.Join("&", dictionary.Select(x => $"{x.Key}={x.Value}"));
            return new StringContent(parametersString, Encoding.UTF8);
        }

        public static string ToQueryString(this Dictionary<string, string> dictionary)
        {
            return "?" + string.Join("&", dictionary.Select(x => $"{x.Key}={x.Value}"));
        }
    }
}