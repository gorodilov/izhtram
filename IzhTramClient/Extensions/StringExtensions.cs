using Newtonsoft.Json;

namespace IzhTramClient.Extensions
{
    public static class StringExtensions
    {
        public static T Convert<T>(this string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }
    }
}