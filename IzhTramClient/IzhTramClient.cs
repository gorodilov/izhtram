using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using IzhTramClient.BusinessObjects;
using IzhTramClient.Extensions;

namespace IzhTramClient
{
    public class IzhTramClient : IIzhTramClient
    {
        private const string url = "http://localhost:5000/api/";
        
        public async Task<Station[]> GetStations()
        {
            var response = await SendRequest(
                HttpMethod.Get, 
                "izhtram/stations", 
                new Dictionary<string, string>()).ConfigureAwait(false);

            return response.Convert<Station[]>();
        }

        public async Task<Route[]> GetRoutes(int stationFromId, int stationToId, int interval)
        {
            var parameters = new Dictionary<string, string>
            {
                {"stationFrom", stationFromId.ToString()},
                {"stationTo", stationToId.ToString()},
                {"interval", interval.ToString()}
            };
            
            var response = await SendRequest(
                HttpMethod.Post, 
                "izhtram/routes", 
                parameters).ConfigureAwait(false);

            return response.Convert<Route[]>();
        }

        public async Task<string> CreateUser(string login, string password)
        {
            var parameters = new Dictionary<string, string>
            {
                {"loginHash", login.GetHashCode().ToString()},
                {"passwordHash", password.GetHashCode().ToString()}
            };
            
            var response = await SendRequest(
                HttpMethod.Post, 
                "user/create", 
                parameters).ConfigureAwait(false);

            return response;
        }

        public async Task<UpdateResult> UpdateUser(string userId, string newPassword)
        {
            var parameters = new Dictionary<string, string>
            {
                {"userId", userId},
                {"newPasswordHash", newPassword.GetHashCode().ToString()}
            };
            
            var response = await SendRequest(
                HttpMethod.Post, 
                "user/update", 
                parameters).ConfigureAwait(false);

            return response.Convert<UpdateResult>();
        }

        public async Task<string> Login(string login, string password)
        {
            var parameters = new Dictionary<string, string>
            {
                {"loginHash", login.GetHashCode().ToString()},
                {"passwordHash", password.GetHashCode().ToString()}
            };
            
            var response = await SendRequest(
                HttpMethod.Get, 
                "user/login", 
                parameters).ConfigureAwait(false);

            return response;
        }

        public async Task<Configuration> GetConfiguration(string userId)
        {
            var parameters = new Dictionary<string, string>
            {
                {"userId", userId}
            };
            
            var response = await SendRequest(
                HttpMethod.Get, 
                "config/get", 
                parameters).ConfigureAwait(false);

            return response.Convert<Configuration>();
        }

        public async Task<Configuration> InitConfiguration(string userId)
        {
            var parameters = new Dictionary<string, string>
            {
                {"userId", userId}
            };
            
            var response = await SendRequest(
                HttpMethod.Post, 
                "config/initialize", 
                parameters).ConfigureAwait(false);

            return response.Convert<Configuration>();
        }

        public async Task<Configuration> UpdateConfiguration(string userId, int newInterval)
        {
            var parameters = new Dictionary<string, string>
            {
                {"userId", userId},
                {"newDefaultInterval", newInterval.ToString()}
            };
            
            var response = await SendRequest(
                HttpMethod.Post, 
                "config/update", 
                parameters).ConfigureAwait(false);

            return response.Convert<Configuration>();
        }

        public Task CreateFavouriteRoute(string userId, int stationFromId, int stationToId)
        {
            var parameters = new Dictionary<string, string>
            {
                {"userId", userId},
                {"stationFrom", stationFromId.ToString()},
                {"stationTo", stationToId.ToString()}
            };

            return SendRequest(HttpMethod.Post, "route/create", parameters);
        }

        public Task DeleteFavouriteRoute(string id)
        {
            var parameters = new Dictionary<string, string>
            {
                {"id", id}
            };

            return SendRequest(HttpMethod.Post, "route/delete", parameters);
        }

        public async Task<FavouriteRoute[]> GetFavouriteRoutes(string userId)
        {
            var parameters = new Dictionary<string, string>
            {
                {"userId", userId}
            };

            var response = await SendRequest(
                HttpMethod.Get, 
                "route/select", 
                parameters).ConfigureAwait(false);
            
            return response.Convert<FavouriteRoute[]>();
        }

        public Task CreateFavouriteTram(string userId, Route tramInfo)
        {
            var parameters = new Dictionary<string, string>
            {
                {"userId", userId},
                {"departure", tramInfo.DepartureString},
                {"arrival", tramInfo.ArrivalString},
                {"tramNumber", tramInfo.Number.ToString()},
            };

            return SendRequest(HttpMethod.Post, "tram/create", parameters);
        }

        public Task DeleteFavouriteTram(string id)
        {
            var parameters = new Dictionary<string, string>
            {
                {"id", id}
            };

            return SendRequest(HttpMethod.Post, "tram/delete", parameters);
        }

        public async Task<FavouriteTram[]> GetFavouriteTrams(string userId)
        {
            var parameters = new Dictionary<string, string>
            {
                {"userId", userId}
            };

            var response = await SendRequest(
                HttpMethod.Get, 
                "tram/select", 
                parameters).ConfigureAwait(false);
            
            return response.Convert<FavouriteTram[]>();
        }

        private static async Task<string> SendRequest(
            HttpMethod httpMethod, 
            string uri, 
            Dictionary<string, string> parameters)
        {
            var request = new HttpRequestMessage(httpMethod, url + uri + parameters.ToQueryString());

            HttpResponseMessage response;
            using (var client = new HttpClient())
            {
                response = await client.SendAsync(request).ConfigureAwait(false);
            }

            var responseContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return responseContent;
        }
    }
}