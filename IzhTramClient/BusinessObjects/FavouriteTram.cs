using Newtonsoft.Json;

namespace IzhTramClient.BusinessObjects
{
    public class FavouriteTram
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("userId")]
        public string UserId { get; set; }
        
        [JsonProperty("departure")]
        public string Departure { get; set; }
        
        [JsonProperty("arrival")]
        public string Arrival { get; set; }
        
        [JsonProperty("isWeekday")]
        public bool IsWeekday { get; set; }
        
        [JsonProperty("tramNumber")]
        public int TramNumber { get; set; }
    }
}