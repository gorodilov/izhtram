using Newtonsoft.Json;

namespace IzhTramClient.BusinessObjects
{
    public class FavouriteRoute
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("userId")]
        public string UserId { get; set; }
        
        [JsonProperty("stationFrom")]
        public int StationFrom { get; set; }
        
        [JsonProperty("stationTo")]
        public int StationTo { get; set; }
    }
}