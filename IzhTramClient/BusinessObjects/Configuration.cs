using Newtonsoft.Json;

namespace IzhTramClient.BusinessObjects
{
    public class Configuration
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("userId")]
        public string UserId { get; set; }
        
        [JsonProperty("defaultInterval")]
        public int DefaultInterval { get; set; }
        
        [JsonProperty("useNowDate")]
        public bool UseNowDate { get; set; }
    }
}