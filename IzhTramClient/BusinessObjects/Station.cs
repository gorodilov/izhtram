using Newtonsoft.Json;

namespace IzhTramClient.BusinessObjects
{
    public class Station
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("title")]
        public string Title { get; set; }
    }
}