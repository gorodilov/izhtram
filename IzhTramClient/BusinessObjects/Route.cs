using Newtonsoft.Json;

namespace IzhTramClient.BusinessObjects
{
    public class Route
    {
        [JsonProperty("number")]
        public int Number { get; set; }
        
        [JsonProperty("departureString")]
        public string DepartureString { get; set; }
        
        [JsonProperty("arrivalString")]
        public string ArrivalString { get; set; }
    }
}