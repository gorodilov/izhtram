using System.Threading.Tasks;
using IzhTramClient.BusinessObjects;

namespace IzhTramClient
{
    public interface IIzhTramClient
    {
        #region IzhTram

        Task<Station[]> GetStations();
        Task<Route[]> GetRoutes(int stationFromId, int stationToId, int interval);

        #endregion
        
        #region User

        Task<string> CreateUser(string login, string password);
        Task<UpdateResult> UpdateUser(string userId, string newPassword);
        Task<string> Login(string login, string password);

        #endregion

        #region UserConfig

        Task<Configuration> GetConfiguration(string userId);
        Task<Configuration> InitConfiguration(string userId);
        Task<Configuration> UpdateConfiguration(string userId, int newInterval);

        #endregion

        #region FavouriteRoute

        Task CreateFavouriteRoute(string userId, int stationFromId, int stationToId);
        Task DeleteFavouriteRoute(string id);
        Task<FavouriteRoute[]> GetFavouriteRoutes(string userId);

        #endregion

        #region FavouriteTram

        Task CreateFavouriteTram(string userId, Route tramInfo);
        Task DeleteFavouriteTram(string id);
        Task<FavouriteTram[]> GetFavouriteTrams(string userId);

        #endregion

    }
}