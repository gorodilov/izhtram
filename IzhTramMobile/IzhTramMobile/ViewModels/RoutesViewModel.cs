﻿using System.Collections.ObjectModel;
using IzhTramMobile.Models;

namespace IzhTramMobile.ViewModels
{
    public class RoutesViewModel
    {
        public ObservableCollection<RouteModel> Routes { get; set; }

        public RoutesViewModel()
        {
        }
    }
}