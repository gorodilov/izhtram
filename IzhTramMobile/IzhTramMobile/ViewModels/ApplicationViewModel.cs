﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using IzhTramClient.BusinessObjects;
using IzhTramMobile.Models;

namespace IzhTramMobile.ViewModels
{
    public class ApplicationViewModel
    {
        public ObservableCollection<FavouriteRouteModel> FavouriteRoutes { get; set; }
        public ObservableCollection<FavouriteTramModel> FavouriteTrams { get; set; }
        public StationModel[] Stations { get; set; }
        public ConfigurationModel Config { get; set; }

        public ApplicationViewModel()
        {
            FavouriteRoutes = new ObservableCollection<FavouriteRouteModel>();
            FavouriteTrams = new ObservableCollection<FavouriteTramModel>();
            Stations = Array.Empty<StationModel>();
            Config = new ConfigurationModel(new Configuration());
        }

        public ApplicationViewModel(
            FavouriteRoute[] favouriteRoutes, 
            FavouriteTram[] favouriteTrams, 
            Station[] stations,
            Configuration configuration)
        {
            Stations = stations.Select(x => new StationModel(x)).ToArray();

            var favouriteRouteModels = favouriteRoutes
                .Select(x => new FavouriteRouteModel(x, Stations))
                .ToArray();

            FavouriteRoutes = new ObservableCollection<FavouriteRouteModel>(favouriteRouteModels);

            var favouriteTramModels = favouriteTrams
                .Select(x => new FavouriteTramModel(x))
                .ToArray();

            FavouriteTrams = new ObservableCollection<FavouriteTramModel>(favouriteTramModels);

            Config = new ConfigurationModel(configuration);
        }
    }
}