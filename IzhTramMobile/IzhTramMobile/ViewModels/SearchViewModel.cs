﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using IzhTramMobile.Models;

namespace IzhTramMobile.ViewModels
{
    public class SearchViewModel
    {
        public ObservableCollection<RouteModel> Routes { get; set; }
        public StationModel[] Stations { get; set; }
        public StationModel SelectedStationFrom { get; set; }
        public StationModel SelectedStationTo { get; set; }

        public SearchViewModel(StationModel[] stations)
        {
            Stations = stations.OrderBy(x => x.Title).ToArray();
            Routes = new ObservableCollection<RouteModel>();
            SelectedStationFrom = Stations.First();
            SelectedStationTo = Stations.First();
        }

        public SearchViewModel()
        {
            Routes = new ObservableCollection<RouteModel>();
            Stations = Array.Empty<StationModel>();
            SelectedStationFrom = new StationModel();
            SelectedStationTo = new StationModel();
        }
    }
}