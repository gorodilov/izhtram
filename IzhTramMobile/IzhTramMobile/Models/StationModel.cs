﻿using IzhTramClient.BusinessObjects;

namespace IzhTramMobile.Models
{
    public class StationModel
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public StationModel(Station station)
        {
            Id = station.Id;
            Title = station.Title;
        }

        public StationModel()
        {
            
        }
    }
}