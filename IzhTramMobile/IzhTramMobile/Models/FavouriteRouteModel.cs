﻿using System.Linq;
using IzhTramClient.BusinessObjects;

namespace IzhTramMobile.Models
{
    public class FavouriteRouteModel
    {
        public string Id { get; set; }
        public string StationFrom { get; set; }
        public string StationTo { get; set; }

        public FavouriteRouteModel(FavouriteRoute favouriteRoute, StationModel[] stations)
        {
            Id = favouriteRoute.Id;
            StationFrom = stations.First(x => x.Id == favouriteRoute.StationFrom).Title;
            StationTo = stations.First(x => x.Id == favouriteRoute.StationTo).Title;
        }
    }
}