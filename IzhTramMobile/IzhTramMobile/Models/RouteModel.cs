﻿using IzhTramClient.BusinessObjects;

namespace IzhTramMobile.Models
{
    public class RouteModel
    {
        public int TramNumber { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }

        public RouteModel(Route route)
        {
            TramNumber = route.Number;
            Departure = route.DepartureString;
            Arrival = route.ArrivalString;
        }

        public RouteModel()
        {
        }
    }
}