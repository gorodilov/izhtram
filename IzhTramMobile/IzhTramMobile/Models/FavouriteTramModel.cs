﻿using IzhTramClient.BusinessObjects;

namespace IzhTramMobile.Models
{
    public class FavouriteTramModel
    {
        public string Id { get; set; }
        public int Number { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }
        public bool isWeekday { get; set; }

        public FavouriteTramModel(FavouriteTram favouriteTram)
        {
            Id = favouriteTram.Id;
            Number = favouriteTram.TramNumber;
            Departure = favouriteTram.Departure;
            Arrival = favouriteTram.Arrival;
            isWeekday = favouriteTram.IsWeekday;
        }
    }
}