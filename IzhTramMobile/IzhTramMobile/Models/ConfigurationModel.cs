﻿using System.ComponentModel;
using IzhTramClient.BusinessObjects;

namespace IzhTramMobile.Models
{
    public class ConfigurationModel
    {
        public string UserId { get; set; }
        public int Interval { get; set; }
        public bool UseNowDate { get; set; }

        public ConfigurationModel(Configuration configuration)
        {
            UserId = configuration.UserId;
            Interval = configuration.DefaultInterval;
            UseNowDate = configuration.UseNowDate;
        }
    }
}