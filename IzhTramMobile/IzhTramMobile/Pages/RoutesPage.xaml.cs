﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using IzhTramMobile.Models;
using IzhTramMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IzhTramMobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RoutesPage : ContentPage
    {
        public ObservableCollection<string> Items { get; set; }

        public RoutesPage(ApplicationViewModel viewModel, RouteModel[] routes)
        {
            Title = "Список рейсов";

            InitializeComponent();

            var routesViewModel = new RoutesViewModel
            {
                Routes = new ObservableCollection<RouteModel>(routes)
            };

            BindingContext = routesViewModel;
        }

        private void RouteItemTapped(object sender, ItemTappedEventArgs e)
        {
            DisplayAlert("Внимание", "Еще один рейс добавлен в избранные", "OK");
        }
    }
}
