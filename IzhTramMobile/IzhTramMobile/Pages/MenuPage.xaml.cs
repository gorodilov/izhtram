﻿using System;
using IzhTramMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IzhTramMobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuPage : ContentPage
    {
        private ApplicationViewModel ViewModel;

        public MenuPage(ApplicationViewModel viewModel)
		{
            this.ViewModel = viewModel;
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);

            InitializeComponent();

            SearchButton.Clicked += SearchButtonClickAsync;
            FavouriteRoutesButton.Clicked += FavouriteRoutesButtonClickAsync;
            FavouriteTramButton.Clicked += FavouriteTramButtonClickAsync;
            ConfigButton.Clicked += ConfigButtonClickAsync;
        }

        private async void ConfigButtonClickAsync(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ConfigPage(ViewModel));
        }

        private async void FavouriteTramButtonClickAsync(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new FavouriteTramsPage(ViewModel));
        }

        private async void FavouriteRoutesButtonClickAsync(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new FavouriteRoutesPage(ViewModel));
        }

        private async void SearchButtonClickAsync(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new RouteSearchPage(ViewModel));
        }
	}
}