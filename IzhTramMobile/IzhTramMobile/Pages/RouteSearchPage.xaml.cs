﻿using System;
using System.Linq;
using System.Threading.Tasks;
using IzhTramMobile.Models;
using IzhTramMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IzhTramMobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RouteSearchPage : ContentPage
    {
        public ApplicationViewModel ViewModel;
        public SearchViewModel SearchViewModel;

		public RouteSearchPage(ApplicationViewModel viewModel)
		{
			InitializeComponent();

            ViewModel = viewModel;

            SearchViewModel = new SearchViewModel(viewModel.Stations);

            BindingContext = SearchViewModel;
        }

        private async void SearchAsync(object sender, EventArgs e)
        {
            WaitSpinner.IsRunning = true;
            var stationFromId = SearchViewModel.SelectedStationFrom.Id;
            var stationToId = SearchViewModel.SelectedStationTo.Id;

            var izhTramClient = new IzhTramClient.IzhTramClient();
            var routes = await izhTramClient.GetRoutes(stationFromId, stationToId, ViewModel.Config.Interval);

            var routeModels = routes.Select(x => new RouteModel(x)).ToArray();

            await Navigation.PushAsync(new RoutesPage(ViewModel, routeModels)).ConfigureAwait(false);
        }
    }
}