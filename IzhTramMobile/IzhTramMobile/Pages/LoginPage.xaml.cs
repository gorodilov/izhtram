﻿using System;
using IzhTramClient.BusinessObjects;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using IzhTramMobile.ViewModels;

namespace IzhTramMobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
        private IzhTramClient.IzhTramClient izhTramClient;

        public LoginPage ()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
			InitializeComponent();

            izhTramClient = new IzhTramClient.IzhTramClient();

            SignIn.Clicked += LoginAsync;
            Registration.Clicked += RegisterAsync;
        }

        private async void LoginAsync(object sender, EventArgs args)
        {
            WaitSpinner.IsRunning = true;
            var userId = await izhTramClient.Login(Login.Text, Password.Text);

            if (userId == string.Empty)
            {
                WaitSpinner.IsRunning = false;
                await DisplayAlert("Ошибка входа", "Неверный логин или пароль", "OK");
                return;
            }

            var favouriteRoutes = await izhTramClient.GetFavouriteRoutes(userId);
            var favouriteTrams = await izhTramClient.GetFavouriteTrams(userId);
            var configuration = await izhTramClient.GetConfiguration(userId);
            var stations = await izhTramClient.GetStations();

            var viewModel = new ApplicationViewModel(favouriteRoutes, favouriteTrams, stations, configuration);

            WaitSpinner.IsRunning = false;
            await Navigation.PushAsync(new MenuPage(viewModel));
        }

        private async void RegisterAsync(object sender, EventArgs args)
        {
            WaitSpinner.IsRunning = true;
            var userId = await izhTramClient.CreateUser(Login.Text, Password.Text);

            var configuration = await izhTramClient.InitConfiguration(userId);
            var stations = await izhTramClient.GetStations();

            var viewModel = new ApplicationViewModel(
                Array.Empty<FavouriteRoute>(), 
                Array.Empty<FavouriteTram>(), 
                stations, 
                configuration);

            WaitSpinner.IsRunning = false;
            await Navigation.PushAsync(new MenuPage(viewModel));
        }
	}
}