namespace IzhTram.DbConfig
{
    public class DbCollections
    {
        public const string Users = "Users";
        public const string UserConfigs = "UserConfigs";
        public const string UserFavouriteRoutes = "UserFavouriteRoutes";
        public const string UserFavouriteTrams = "UserFavouriteTrams";
    }
}