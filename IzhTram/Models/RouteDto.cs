namespace IzhTram.Models
{
    public class RouteDto
    {
        public int Number { get; set; }
        public string DepartureString { get; set; }
        public string ArrivalString { get; set; }
    }
}