using System.Collections.Generic;
using IzhTram.Entities;
using IzhTram.Handlers;

namespace IzhTram.Implementation.Services
{
    public interface IUserFavouriteRouteService
    {
        void Create(string userId, int stationFromId, int stationToId);
        void Delete(string id);
        List<UserFavouriteRouteDbo> Select(string userId);
    }
    
    public class UserFavouriteRouteService : IUserFavouriteRouteService
    {
        private readonly IUserFavouriteRouteHandler _userFavouriteRouteHandler;

        public UserFavouriteRouteService(IUserFavouriteRouteHandler userFavouriteRouteHandler)
        {
            _userFavouriteRouteHandler = userFavouriteRouteHandler;
        }
        
        public void Create(string userId, int stationFromId, int stationToId)
        {
            var userFavouriteRoute = new UserFavouriteRouteDbo
            {
                UserId = userId,
                StationFrom = stationFromId,
                StationTo = stationToId
            };

            _userFavouriteRouteHandler.Create(userFavouriteRoute);
        }

        public void Delete(string id)
        {
            _userFavouriteRouteHandler.Delete(id);
        }

        public List<UserFavouriteRouteDbo> Select(string userId)
        {
            return _userFavouriteRouteHandler.SelectByUserId(userId);
        }
    }
}