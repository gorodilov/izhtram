using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IzhTram.Implementation.Parsers;
using IzhTram.Models;

namespace IzhTram.Implementation.Services
{
    public interface IIzhgetService
    {
        Task<StationDto[]> GetStationsAsync();

        Task<RouteDto[]> GetRoutesAsync(
            int stationFromId,
            int stationToId,
            int interval,
            DateTime date);
    }
    
    public class IzhgetService : IIzhgetService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IIzhgetResponseParser _izhgetResponseParser;
        private HttpClient httpClient => _httpClientFactory.CreateClient();

        public IzhgetService(
            IHttpClientFactory httpClientFactory, 
            IIzhgetResponseParser izhgetResponseParser)
        {
            _httpClientFactory = httpClientFactory;
            _izhgetResponseParser = izhgetResponseParser;
        }
        
        public async Task<StationDto[]> GetStationsAsync()
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "http://xn--c1aff6b0c.xn--p1ai/rasp/list_station.php");
            var response = await httpClient.SendAsync(request).ConfigureAwait(false);

            var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return _izhgetResponseParser.ParseStations(result);
        }

        public async Task<RouteDto[]> GetRoutesAsync(int stationFromId, int stationToId, int interval, DateTime date)
        {
            var dateString = date.ToString("dd.MM.yyyy");
            var hours = date.ToString("HH");
            var minutes = date.ToString("mm");
            
            var parametersString = "route=0" +
                                   $"&stn={stationFromId}" +
                                   $"&dstn={stationToId}" +
                                   $"&timeint={interval}" +
                                   $"&dt={dateString}" +
                                   $"&th_rasp={hours}" +
                                   $"&tm_rasp={minutes}";
            
            var requestUrl = $"http://xn--c1aff6b0c.xn--p1ai/rasp/load_station.php";

            var request = new HttpRequestMessage(HttpMethod.Post, requestUrl)
            {
                Content = new StringContent(parametersString, Encoding.UTF8, "application/x-www-form-urlencoded")
            };

            var response = await httpClient.SendAsync(request).ConfigureAwait(false);

            var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return _izhgetResponseParser.ParseRoutes(result);
        }
    }
}