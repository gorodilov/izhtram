using IzhTram.Entities;
using IzhTram.Handlers;
using IzhTram.Models;
using MongoDB.Bson;

namespace IzhTram.Implementation.Services
{
    public interface IUserService
    {
        string Create(string loginHash, string passwordHash);
        UpdateResult Update(string id, string passwordHash);
        string Login(string loginHash, string passwordHash);
    }

    public class UserService : IUserService
    {
        private readonly IUserHandler _userHandler;

        public UserService(IUserHandler userHandler)
        {
            _userHandler = userHandler;
        }
        
        public string Create(string loginHash, string passwordHash)
        {
            var user = new UserDbo
            {
                LoginHash = loginHash,
                PasswordHash = passwordHash
            };
            
            return _userHandler.Create(user);
        }

        public UpdateResult Update(string id, string passwordHash)
        {
            var userDbo = _userHandler.Find(id);
            
            if (userDbo != null)
            {
                userDbo.PasswordHash = passwordHash;
                _userHandler.Update(id, userDbo);
                return UpdateResult.Success;
            }

            return UpdateResult.Failed;
        }

        public string Login(string loginHash, string passwordHash)
        {
            var bsonDocument = new BsonDocument("Login", loginHash);
            var userDbo = _userHandler.Find(bsonDocument);

            return userDbo != null && userDbo.PasswordHash == passwordHash ? userDbo.Id : string.Empty;
        }
    }
}