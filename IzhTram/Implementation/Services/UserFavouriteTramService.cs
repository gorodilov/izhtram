using System;
using System.Collections.Generic;
using IzhTram.Entities;
using IzhTram.Handlers;

namespace IzhTram.Implementation.Services
{
    public interface IUserFavouriteTramService
    {
        void Create(string userId, string departure, string arrival, int tramNumber);
        void Delete(string id);
        List<UserFavouriteTramDbo> Select(string userId);
    }
    
    public class UserFavouriteTramService : IUserFavouriteTramService
    {
        private readonly IUserFavouriteTramHandler _userFavouriteTramHandler;

        public UserFavouriteTramService(IUserFavouriteTramHandler userFavouriteTramHandler)
        {
            _userFavouriteTramHandler = userFavouriteTramHandler;
        }
        
        public void Create(string userId, string departure, string arrival, int tramNumber)
        {
            var dayOfWeek = (int)DateTime.Now.DayOfWeek;
            var isWeekday = dayOfWeek >= 1 && dayOfWeek <= 5;

            var userFavouriteTram = new UserFavouriteTramDbo
            {
                UserId = userId,
                Departure = departure,
                Arrival = arrival,
                TramNumber = tramNumber,
                IsWeekday = isWeekday
            };

            _userFavouriteTramHandler.Create(userFavouriteTram);
        }

        public void Delete(string id)
        {
            _userFavouriteTramHandler.Delete(id);
        }

        public List<UserFavouriteTramDbo> Select(string userId)
        {
            return _userFavouriteTramHandler.SelectByUserId(userId);
        }
    }
}