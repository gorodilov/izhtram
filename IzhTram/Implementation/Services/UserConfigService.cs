using IzhTram.Entities;
using IzhTram.Handlers;
using MongoDB.Bson;

namespace IzhTram.Implementation.Services
{
    public interface IUserConfigService
    {
        UserConfigDbo Init(string userId);
        UserConfigDbo Update(string userId, int defaultInterval);
        UserConfigDbo Read(string userId);
    }
    
    public class UserConfigService : IUserConfigService
    {
        private readonly IUserConfigHandler _userConfigHandler;

        public UserConfigService(IUserConfigHandler userConfigHandler)
        {
            _userConfigHandler = userConfigHandler;
        }
        
        public UserConfigDbo Init(string userId)
        {
            var userConfig = new UserConfigDbo
            {
                UserId = userId,
                DefaultInterval = 30,
                UseNowDate = true
            };

            var configId = _userConfigHandler.Create(userConfig);
            
            return _userConfigHandler.Find(configId);
        }

        public UserConfigDbo Update(string userId, int defaultInterval)
        {
            var userConfig = Read(userId);

            userConfig.DefaultInterval = defaultInterval;

            _userConfigHandler.Update(userConfig.Id, userConfig);

            return userConfig;
        }

        public UserConfigDbo Read(string userId)
        {
            return _userConfigHandler.Find(new BsonDocument("UserId", new ObjectId(userId)));
        }
    }
}