using IzhTram.Models;

namespace IzhTram.Implementation.Parsers
{
    public interface IIzhgetResponseParser
    {
        StationDto[] ParseStations(string stationsHtmlString);
        RouteDto[] ParseRoutes(string routesHtmlString);
    }
}