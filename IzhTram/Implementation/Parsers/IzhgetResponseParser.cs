using System.Linq;
using HtmlAgilityPack;
using IzhTram.Models;

namespace IzhTram.Implementation.Parsers
{
    public class IzhgetResponseParser : IIzhgetResponseParser
    {
        public StationDto[] ParseStations(string stationsHtmlString)
        {
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(stationsHtmlString);

            var stationsNodes = htmlDocument.DocumentNode
                .SelectSingleNode("//select")
                .SelectNodes("//option");

            var stationDtos = stationsNodes
                .Select(x => new StationDto
                {
                    Id = int.Parse(x.Attributes["value"].Value),
                    Title = x.InnerText
                })
                .Where(x => x.Id != 0)
                .ToArray();
            
            return stationDtos;
        }

        public RouteDto[] ParseRoutes(string routesHtmlString)
        {
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(routesHtmlString);
            var selectNode = htmlDocument.DocumentNode.SelectSingleNode("//table");

            var routeNodes = selectNode
                .SelectNodes("//tr")
                .Where(x => x.ChildNodes.Count == 4)
                .Skip(1);
            
            var routesDtos = routeNodes
                .Select(x => x.ChildNodes.Select(y => y.InnerText).ToArray())
                .Select(x => new RouteDto
                {
                    Number = int.Parse(x[0]),
                    DepartureString = x[2],
                    ArrivalString = x[3]
                })
                .ToArray();

            return routesDtos;
        }
    }
}