using IzhTram.DbConfig;
using IzhTram.Entities;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;

namespace IzhTram.Handlers
{
    public interface IUserConfigHandler
    {
        string Create(UserConfigDbo item);
        UserConfigDbo Find(string id);
        UserConfigDbo Find(BsonDocument bsonDocument);
        void Update(string id, UserConfigDbo item);
    }

    public class UserConfigHandler : HandlerBase<UserConfigDbo>, IUserConfigHandler
    {
        public UserConfigHandler(IConfiguration configuration) 
            : base(configuration, DbCollections.UserConfigs)
        {
        }
    }
}