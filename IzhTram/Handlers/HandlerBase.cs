using System.Collections.Generic;
using IzhTram.DbConfig;
using IzhTram.Entities;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace IzhTram.Handlers
{
    public abstract class HandlerBase<T> where T : IEntity
    {
        private readonly IMongoCollection<T> _collection;
        
        protected HandlerBase(IConfiguration configuration, string collectionName)
        {
            var url = new MongoUrl(configuration.GetConnectionString("IzhTramDb"));
            var mongoClient = new MongoClient(url);
            var db = mongoClient.GetDatabase(DatabaseNames.IzhTram);
            _collection = db.GetCollection<T>(collectionName);
        }

        public virtual List<T> SelectByUserId(string userId)
        {
            return _collection.Find(new BsonDocument("UserId", new ObjectId(userId))).ToList();
        }

        public virtual T Find(string id)
        {
            return _collection.Find(x => x.Id == id).SingleOrDefault();
        }

        public virtual T Find(BsonDocument bsonDocument)
        {
            return _collection.Find(bsonDocument).FirstOrDefault();
        }

        public virtual string Create(T item)
        {
            _collection.InsertOne(item);
            return item.Id;
        }

        public virtual void Update(string id, T item)
        {
            _collection.ReplaceOne(x => x.Id == id, item);
        }

        public virtual void Delete(string id)
        {
            _collection.DeleteOne(x => x.Id == id);
        }
    }
}