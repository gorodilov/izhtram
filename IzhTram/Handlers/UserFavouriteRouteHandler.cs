using System.Collections.Generic;
using IzhTram.DbConfig;
using IzhTram.Entities;
using Microsoft.Extensions.Configuration;

namespace IzhTram.Handlers
{
    public interface IUserFavouriteRouteHandler
    {
        string Create(UserFavouriteRouteDbo item);
        void Delete(string id);
        List<UserFavouriteRouteDbo> SelectByUserId(string userId);
    }
    
    public class UserFavouriteRouteHandler : HandlerBase<UserFavouriteRouteDbo>, IUserFavouriteRouteHandler
    {
        public UserFavouriteRouteHandler(IConfiguration configuration) 
            : base(configuration, DbCollections.UserFavouriteRoutes)
        {
        }
    }
}