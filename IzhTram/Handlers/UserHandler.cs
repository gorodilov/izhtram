using IzhTram.DbConfig;
using IzhTram.Entities;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;

namespace IzhTram.Handlers
{
    public interface IUserHandler
    {
        string Create(UserDbo item);
        UserDbo Find(string id);
        UserDbo Find(BsonDocument bsonDocument);
        void Update(string id, UserDbo item);
    }
    
    public class UserHandler : HandlerBase<UserDbo>, IUserHandler
    {
        public UserHandler(IConfiguration configuration)
            : base(configuration, DbCollections.Users)
        {
        }
    }
}