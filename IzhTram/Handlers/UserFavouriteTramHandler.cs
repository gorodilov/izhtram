using System.Collections.Generic;
using IzhTram.Entities;
using Microsoft.Extensions.Configuration;

namespace IzhTram.Handlers
{
    public interface IUserFavouriteTramHandler
    {
        string Create(UserFavouriteTramDbo item);
        void Delete(string id);
        List<UserFavouriteTramDbo> SelectByUserId(string userId);
    }
    
    public class UserFavouriteTramHandler : HandlerBase<UserFavouriteTramDbo>, IUserFavouriteTramHandler
    {
        public UserFavouriteTramHandler(IConfiguration configuration, string collectionName) : base(configuration, collectionName)
        {
        }
    }
}