﻿using IzhTram.Handlers;
using IzhTram.Implementation.Parsers;
using IzhTram.Implementation.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IzhTram
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddHttpClient();

            services.AddTransient(typeof(IIzhgetService), typeof(IzhgetService));
            services.AddTransient(typeof(IIzhgetResponseParser), typeof(IzhgetResponseParser));

            services.AddSingleton<IUserHandler, UserHandler>();
            services.AddTransient(typeof(IUserService), typeof(UserService));

            services.AddSingleton<IUserConfigHandler, UserConfigHandler>();
            services.AddTransient(typeof(IUserConfigService), typeof(UserConfigService));
            
            services.AddSingleton<IUserFavouriteRouteHandler, UserFavouriteRouteHandler>();
            services.AddTransient(typeof(IUserFavouriteRouteService), typeof(UserFavouriteRouteService));
            
            services.AddSingleton<IUserFavouriteTramHandler, UserFavouriteTramHandler>();
            services.AddTransient(typeof(IUserFavouriteTramService), typeof(UserFavouriteTramService));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseMvc();
            
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
        }
    }
}