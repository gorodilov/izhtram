using System;
using System.Threading.Tasks;
using IzhTram.Implementation.Services;
using IzhTram.Models;
using Microsoft.AspNetCore.Mvc;

namespace IzhTram.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IzhTramController : Controller
    {
        private readonly IIzhgetService _izhgetService;
        private readonly IUserService _userService;

        public IzhTramController(IIzhgetService izhgetService, IUserService userService)
        {
            _izhgetService = izhgetService;
            _userService = userService;
        }
        
        [HttpGet("stations")]
        public async Task<ActionResult<StationDto[]>> GetStations()
        {
            return await _izhgetService.GetStationsAsync().ConfigureAwait(true);
        }

        [HttpPost("routes")]
        public async Task<ActionResult<RouteDto[]>> GetRoutes(int stationFrom, int stationTo, int interval)
        {
            return await _izhgetService.GetRoutesAsync(stationFrom, stationTo, interval, DateTime.Now).ConfigureAwait(true);
        }
    }
}