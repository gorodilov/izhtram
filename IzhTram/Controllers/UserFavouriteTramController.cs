using System.Collections.Generic;
using IzhTram.Entities;
using IzhTram.Implementation.Services;
using Microsoft.AspNetCore.Mvc;

namespace IzhTram.Controllers
{
    [Route("api/tram")]
    public class UserFavouriteTramController : Controller
    {
        private readonly IUserFavouriteTramService _userFavouriteTramService;

        public UserFavouriteTramController(IUserFavouriteTramService userFavouriteTramService)
        {
            _userFavouriteTramService = userFavouriteTramService;
        }
        
        [HttpPost("create")]
        public ActionResult Create(string userId, string departure, string arrival, int tramNumber)
        {
            _userFavouriteTramService.Create(userId, departure, arrival, tramNumber);

            return Ok();
        }

        [HttpPost("delete")]
        public ActionResult Delete(string id)
        {
            _userFavouriteTramService.Delete(id);

            return Ok();
        }

        [HttpGet("select")]
        public ActionResult<List<UserFavouriteTramDbo>> Get(string userId)
        {
            return _userFavouriteTramService.Select(userId);
        }
    }
}