using IzhTram.Entities;
using IzhTram.Implementation.Services;
using Microsoft.AspNetCore.Mvc;

namespace IzhTram.Controllers
{
    [Route("api/config")]
    public class UserConfigController : Controller
    {
        private readonly IUserConfigService _userConfigService;

        public UserConfigController(IUserConfigService userConfigService)
        {
            _userConfigService = userConfigService;
        }
        
        [HttpGet("get")]
        public ActionResult<UserConfigDbo> Get(string userId)
        {
            return _userConfigService.Read(userId);
        }
        
        [HttpPost("initialize")]
        public ActionResult<UserConfigDbo> InitializeConfig(string userId)
        {
            return _userConfigService.Init(userId);
        }

        [HttpPost("update")]
        public ActionResult<UserConfigDbo> Update(string userId, int newDefaultInterval)
        {
            return _userConfigService.Update(userId, newDefaultInterval);
        }
    }
}