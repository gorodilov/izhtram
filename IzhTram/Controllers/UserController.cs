using IzhTram.Implementation.Services;
using IzhTram.Models;
using Microsoft.AspNetCore.Mvc;

namespace IzhTram.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        
        [HttpPost("create")]
        public ActionResult<string> Create(string loginHash, string passwordHash)
        {
            return _userService.Create(loginHash, passwordHash);
        }

        [HttpPost("update")]
        public ActionResult<UpdateResult> Update(string id, string newPasswordHash)
        {
            return _userService.Update(id, newPasswordHash);
        }

        [HttpGet("login")]
        public ActionResult<string> Login(string loginHash, string passwordHash)
        {
            return _userService.Login(loginHash, passwordHash);
        }
    }
}