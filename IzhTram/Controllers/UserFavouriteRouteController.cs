using System.Collections.Generic;
using IzhTram.Entities;
using IzhTram.Implementation.Services;
using Microsoft.AspNetCore.Mvc;

namespace IzhTram.Controllers
{
    [Route("api/route")]
    public class UserFavouriteRouteController : Controller
    {
        private readonly IUserFavouriteRouteService _userFavouriteRouteService;

        public UserFavouriteRouteController(IUserFavouriteRouteService userFavouriteRouteService)
        {
            _userFavouriteRouteService = userFavouriteRouteService;
        }
        
        [HttpPost("create")]
        public ActionResult Create(string userId, int stationFrom, int stationTo)
        {
            _userFavouriteRouteService.Create(userId, stationFrom, stationTo);

            return Ok();
        }
        
        [HttpPost("delete")]
        public ActionResult Delete(string id)
        {
            _userFavouriteRouteService.Delete(id);

            return Ok();
        }

        [HttpGet("select")]
        public ActionResult<List<UserFavouriteRouteDbo>> Get(string userId)
        {
            return _userFavouriteRouteService.Select(userId);
        }
    }
}