using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace IzhTram.Entities
{
    public class UserFavouriteTramDbo : IEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [BsonElement("UserId")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }
        
        [BsonElement("Departure")]
        public string Departure { get; set; }
        
        [BsonElement("Arrival")]
        public string Arrival { get; set; }
        
        [BsonElement("IsWeekday")]
        public bool IsWeekday { get; set; }
        
        [BsonElement("TramNumber")]
        public int TramNumber { get; set; }
    }
}