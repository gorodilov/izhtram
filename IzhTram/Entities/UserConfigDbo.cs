using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace IzhTram.Entities
{
    public class UserConfigDbo : IEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [BsonElement("UserId")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }
        
        [BsonElement("DefaultInterval")]
        public int DefaultInterval { get; set; }
        
        [BsonElement("UseNowDate")]
        public bool UseNowDate { get; set; }
    }
}