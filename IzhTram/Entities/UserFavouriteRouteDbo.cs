using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace IzhTram.Entities
{
    public class UserFavouriteRouteDbo : IEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [BsonElement("UserId")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }
        
        [BsonElement("StationFrom")]
        public int StationFrom { get; set; }
        
        [BsonElement("StationTo")]
        public int StationTo { get; set; }
    }
}