using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace IzhTram.Entities
{
    public class UserDbo : IEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [BsonElement("Login")]
        public string LoginHash { get; set; }
        
        [BsonElement("Password")]
        public string PasswordHash { get; set; }
    }
}